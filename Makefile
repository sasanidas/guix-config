# Makefile, tangles all configs and builds a new guix generation
# Dustin Lyons

SHELL = /bin/sh
CYAN_TERMINAL_OUTPUT = \033[1;36m
GREEN_TERMINAL_OUTPUT = \033[1;32m
RED_TERMINAL_OUTPUT = \033[1;31m
CLEAR = \033[0m

## Config Targets - Tangle literate config into real conf and shell files
## =============================================================================
home: desktop
	@{ \
		echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Building Felix...${CLEAR}"
		emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "Workstation-Desktop-Felix.org")'
	}

manifest: 
	@{ \
		echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Building Manifest...${CLEAR}"
		emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "manifest.org")'
	}

system: 
	@{ \
		echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Building Base System...${CLEAR}"
		emacs --batch --eval "(require 'org)" --eval '(org-babel-tangle-file "system.org")'
		echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Moving channels...${CLEAR}"
		mkdir -p ~/.config/guix/
		if cp build/channels.scm ~/.config/guix/channels.scm; then
			 echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] New channels in place${CLEAR}"
		fi
	}


## Deployment Targets - runs guix reconfigure to install new OS generation
## =============================================================================
.ONESHELL:
--deploy-felix-system:
	@{ \
		echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Deploying Guix System...${CLEAR}"
		if sudo -E guix system --cores=12 --load-path=./build reconfigure ./build/felix-os.scm; then
			echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Finished deploying Guix System.${CLEAR}"
		fi
	}

.ONESHELL:
--deploy-felix-home:
	@{ \
		echo -e "${GREEN_TERMINAL_OUTPUT}--> Deploying Guix Home...${CLEAR}"
		if guix home --load-path=./build reconfigure ./build/felix-home.scm; then
			 echo -e "${GREEN_TERMINAL_OUTPUT}--> [Makefile] Finished deploying Guix Home.${CLEAR}"
		fi
	}
